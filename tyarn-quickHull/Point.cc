#include "Point.h"
#include "LinkedList.h"
#include <iostream>
using namespace std;

//************************************
//Start Constructors
//************************************

//PRE:
//POST:creates an empty Point
Point::Point(){}

  //PRE:
  //POST:pX and pY are of the same type
Point::Point(int pX, int pY){
    x = pX;
    y = pY;
}

  //PRE:
  //POST:makes a deep copy of Point object
Point::Point(const Point & pPoint){
  x = pPoint.x;
  y = pPoint.y;
}


  //************************************
  //End Constructors
  //************************************

  //************************************
  //Start Member Functions
  //************************************


  //PRE:both Point objects satisfy the CI
  //POST:returns true if the implicit parameter is farther to the right than the explicit
bool Point::leftMost(const Point & pPoint) const {
    bool returnVal = true; 
    if(x > pPoint.x){
      returnVal = false;
    }
    return returnVal;
  }


  //PRE:both Point objects satisfy the CI
  //POST:returns true if the implicit parameter is farther to the right than the explicit
bool Point::rightMost(const Point & pPoint) const {
    bool returnVal = true; 
    if(x < pPoint.x){
      returnVal = false;
    }
    return returnVal;
  }


  //PRE:Point satisfies CI
  //POST:returns the x coor of point
int Point::getX(){
  return x;
}

  //PRE:Point satisfies CI
  //POST:returns the y coor of point
int Point::getY(){
  return y;
}

  

  //************************************
  //End Member Functions
  //************************************





  //************************************
  //Start Overloaded Operators
  //************************************


  //PRE:both Point objects satisfy the CI
  //POST:compares the y values of each point and returns true if the implicit has a larger y value.
bool  Point::operator > (const Point & pPoint) const {
    bool returnVal = true;
    if(y < pPoint.y){
      returnVal = false;
    }
    return returnVal;
  }

  //PRE:both Point objects satisfy the CI
  //POST:compares the y values of each point and returns true if the implicit has a smaller y value
bool Point::operator < (const Point & pPoint) const {
    bool returnVal = true;
    if(y > pPoint.y){
      returnVal = false;
    }
    return returnVal;
  }



  //PRE:both Point objects satisfy the CI
  //POST:returns true if every private member data value is the same in each Point object, false otherwise.
bool Point::operator == (const Point & pPoint) const {
  bool returnVal;
  if( (x == pPoint.x) && (y == pPoint.y)){
    returnVal = true;
  }
  else{
    returnVal = false;
  }
  return returnVal;
}
  

//PRE:both Point objects satisfy the CI
  //PRE:returns true if the objects are not equal and false if they are equal
bool Point::operator != (const Point & pPoint) const {
  bool returnVal;
  if( (x != pPoint.x) || (y != pPoint.y)){
    returnVal = true;
  }
  else{
    returnVal = false;
  }
  return returnVal;
}



  //PRE:Point satisfies CI
  //POST:prints out the point
ostream & operator << (ostream & stream, const Point & pPoint){
  stream << "(" << pPoint.x << "," << pPoint.y << ")";
  return (stream);
}

 
  //************************************
  //End Overloaded Operators
  //************************************
