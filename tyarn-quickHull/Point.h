#ifndef INCLUDED_POINT
#define INCLUDED_POINT
#include <iostream>
using namespace std;

class Point{
  //CI x is defined as well as y as ints.
 private:
   int x;
   int y;
 public:


  //************************************
  //Start Constructors
  //************************************
   //PRE:
   //POST:creates an empty Point
   Point();
   
   //PRE:
   //POST:pX and pY are of the same type
   Point(int pX, int pY);

  //PRE:
  //POST:makes a deep copy of Point object
  Point(const Point & pPoint);


  //************************************
  //End Constructors
  //************************************

  //************************************
  //Start Member Functions
  //************************************


  //PRE:both Point objects satisfy the CI
  //POST:returns true if the implicit parameter is farther to the right than the explicit
  bool leftMost(const Point & pPoint) const;


  //PRE:both Point objects satisfy the CI
  //POST:returns true if the implicit parameter is farther to the right than the explicit
  bool rightMost(const Point & pPoint) const;

  //PRE:Point satisfies CI
  //POST:returns the x coor of point
  int getX();

  //PRE:Point satisfies CI
  //POST:returns the y coor of point
  int getY();

  
  //************************************
  //End Member Functions
  //************************************





  //************************************
  //Start Overloaded Operators
  //************************************


  //PRE:both Point objects satisfy the CI
  //POST:compares the y values of each point and returns true if the implicit has a larger y value.
  bool  operator > (const Point & pPoint) const;

  //PRE:both Point objects satisfy the CI
  //POST:compares the y values of each point and returns true if the implicit has a smaller y value
  bool operator < (const Point & pPoint) const;


  //PRE:both Point objects satisfy the CI
  //POST:returns true if every private member data value is the same in each Point object, false otherwise.
  bool operator == (const Point & pPoint) const;

  //PRE:both Point objects satisfy the CI
  //PRE:returns true if the objects are not equal and false if they are equal
  bool operator != (const Point & pPoint) const;
  

  //PRE:Point satisfies CI
  //POST:prints out the point
  friend ostream & operator << (ostream & stream, const Point & pPoint);

 
  //************************************
  //End Overloaded Operators
  //************************************

};
#endif
