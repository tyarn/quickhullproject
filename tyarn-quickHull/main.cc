#include "LinkedList.h"
#include "Point.h"
#include "helper.h"
#include <iostream>
#include <fstream>
#define THREE 3
using namespace std;


//PRE:
//POST:argc is number of arguments on cmd line, argv[1] is input file argv[2] is output file
//performs functions  designated to main, more information in readme that is  sent with this file.
int main(int argc, char * argv[]){
  if((argv[1] != NULL) && (argv[2] != NULL) && (argc == THREE)){
    ifstream inputFile;
    inputFile.open(argv[1]); //use inputFile.eof to check if it has reached end, it returns true if it has reached end.
    ofstream outputFile;
    outputFile.open(argv[2]);

    LinkedList <Point> pointList;
    int firstNum;
    int secondNum;
    inputFile >> firstNum;
    inputFile >> secondNum;
    while(!inputFile.eof()){
      Point pPoint(firstNum, secondNum);
      pointList.addToBack(pPoint);
      inputFile >> firstNum;
      inputFile >> secondNum;
    }
    LinkedList <Point> convexHull;
    convexHull = quickHull(pointList);//perform quickhull. 
    int numNodes = convexHull.getNumNodes();
    for(int index = 0; index < numNodes; index ++){//write answer to outputfile. 
      outputFile<<convexHull.getNth(index)<<endl;
    }
  }
  else{
    cout<<"There is a problem with the files on the command line, either there are not two, one for reading and one for outputting, one or both of the files do not have read permissions."<<endl;
  }
  return(0); 
}


//only thing to do now is to delete all the couts and write the readme.
