#ifndef INCLUDED_HELPER
#define INCLUDED_HELPER
#include "Point.h"
#include "LinkedList.h"



//need function to go through linkedlist and find the leftmost point
//PRE:The LinkedList object specified must be of the type Point.
//POST:returns the rightmost point in a linked list.
Point returnRightMost(const LinkedList <Point> & pLinkedList);

//need function to go through linkedlist and find rightmost point.
//PRE:The LinkedList object specified must be of the type Point.
//POST:returns the leftmost point in a linked list.
Point returnLeftMost(const LinkedList <Point> & pLinkedList);



//PRE:The LinkedList object specified must be of the type Point
//POST:returns the highest(y-value wise) point in the linkedList
Point returnHighest(const LinkedList <Point> & pLinkedList);


//PRE:The LinkedList object specified must be of the type Point
//POST:returns the lowest(y-value wise) point in the linkedList
Point returnLowest(const LinkedList <Point> & pLinkedList);


//PRE:all of these Point objects satisfy the CI for that class
//POST:returns 1 if the pPoint is above, -1 if it is below, and 0 if it is on the line.
int aboveOrBelow(Point leftMost, Point rightMost, Point pPoint);

//PRE:all three LinkedLists are of type Point and satisfy the CI, leftMost and rightMost are points representing the leftmost and rightmost points in a linkedList.below and above must be passed by reference because they are being changed.
//POST:puts all points that are below the line created be leftmost and rightmost into one linkedlist and all others above into another linkedlist.
void allocateAboveOrBelow(const LinkedList <Point> & original, LinkedList <Point> & below, LinkedList <Point> & above, Point leftMost, Point rightMost);

//PRE:LinkedList satisfies the CI and has 3 or more nodes in it and is of type point
//POST:prints out the convex hull of a group of points.
LinkedList <Point> quickHull(const LinkedList <Point> & pLinkedList);

//PRE:pLinkedList is a linkedList of points that satisfies the CI, leftmost is the leftmost point in the linked list, rightMost is the rightmos
//POST:recursively calls this function to get the convex hull of points above the line created by leftMost and rightMost point.
LinkedList <Point> quickHullAbove(const LinkedList<Point> & pLinkedList,Point leftMost,Point rightMost);



//PRE:pLinkedList is a linkedList of points that satisfies the CI, leftmost is the leftmost point in the linked list, rightMost is the rightmos
//POST:recursively calls this function to get the convex hull of points below the line created by leftMost and rightMost point.
LinkedList <Point> quickHullBelow(const LinkedList<Point> & pLinkedList,Point leftMost,Point rightMost);



//CHECKLINES HOLDER
//NEED FUNCTION TO CHECK IF A POINT IS ON A LINE OF THE CONVEX HULL, SO FOR EVERY POINT IN THE ORIGINAL LIST IF IT ISNT IN THE CONVEX HULL CHECK IF IT IS ON A LINE CREATED BY TWO POINTS MADE BY THE CONVEX HULL THEN ADD IT? IS THIS EVEN NEEDED THOUGH CAUSE YOU CAN MAKE CONVEX HULL WITHOUT IT ASK SHENDE



//QUICKHULL HELPER PLACEHOLDER I have it kinda written/ drawn out in notebook. Essentially in the quickHull you find the
//leftmost and rightmost points, then do a forloop with the above/below if statement, if its above the line put in another
//linkedlist, same for below, then call the helper twice, once with the above linkedlist and again with the below linkedlist, along with the leftmost and rightmost point.
//then in this you find the max below or above, max below for below linkedlist and vice versa, so you need like a bool or something that says look for below or above in this helper.
//then u find all the points above or below, depending on which function call u in, the line made with the max point and the leftmost/rightmost. ect ect
//your base case is when you put in a linkedlist with 1 point, or 0, in that case you just return the linkedlist.
#endif
