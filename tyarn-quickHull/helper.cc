#include "helper.h"
#include "LinkedList.h"
#define THREE 3
#include <iostream>
using namespace std;

//PRE:The LinkedList object specified must be of the type Point.
//POST:returns the rightmost point from a linkedlist.
Point returnRightMost(const LinkedList <Point> & pLinkedList){
  int index = 1;//cause we gonna get the firstPoint as rightMost
  int numNodes = pLinkedList.getNumNodes();
  Point rightPoint = pLinkedList.getNth(0);
  for(index; index < numNodes; index++){
    if(!rightPoint.rightMost(pLinkedList.getNth(index))){
      rightPoint = pLinkedList.getNth(index);//ASSERT:plinkedlist.getNth(index) is more to the right than the original rightpoint
    }
  }
  return rightPoint;
}


//PRE:The LinkedList object specified must be of the type Point.
//POST:returns the leftmost point in from a linkedlist.
Point returnLeftMost(const LinkedList <Point> & pLinkedList){
  int index = 1;//cause we gonna get the firstPoint as leftMost
  int numNodes = pLinkedList.getNumNodes();
  Point leftPoint = pLinkedList.getNth(0);
  for(index; index < numNodes; index++){
    if(!leftPoint.leftMost(pLinkedList.getNth(index))){
      leftPoint = pLinkedList.getNth(index);//ASSERT:plinkedlist.getNth(index) is more to the left than the original leftPoint
    }
  }
  return leftPoint;
}

//PRE:The LinkedList object specified must be of the type Point
//POST:returns the highest(y-value wise) point in the linkedList
Point returnHighest(const LinkedList <Point> & pLinkedList){
  int index = 1;//cause we gonna get the firstPoint as highest
  int numNodes = pLinkedList.getNumNodes();
  Point highest = pLinkedList.getNth(0);
  for(index; index < numNodes; index++){
    if(highest < pLinkedList.getNth(index)){
      highest = pLinkedList.getNth(index);//ASSERT:getNth(index) is higher than highest
    }
  }
  return highest;
}

//PRE:The LinkedList object specified must be of the type Point
//POST:returns the lowest(y-value wise) point in the linkedList
 Point returnLowest(const LinkedList <Point> & pLinkedList){
  int index = 1;//cause we gonna get the firstPoint as highest
  int numNodes = pLinkedList.getNumNodes();
  Point lowest = pLinkedList.getNth(0);
  for(index; index < numNodes; index++){
    if(lowest > pLinkedList.getNth(index)){
      lowest = pLinkedList.getNth(index);//ASSERT:getNth(index) is lower than lowest
    }
  }
  return lowest;
}



//PRE:all of these Point objects satisfy the CI for that class
//POST:returns 1 if the pPoint is above, -1 if it is below, and 0 if it is on the line.
int aboveOrBelow(Point leftMost, Point rightMost, Point pPoint){
  int aX = leftMost.getX();
  int aY = leftMost.getY();
  int bX = rightMost.getX();
  int bY = rightMost.getY();
  int cX = pPoint.getX();
  int cY = pPoint.getY(); //ASSERT:all x and y values from all points have been allocated
  int eq = ( ((bX - aX) * (cY -aY)) - ((bY - aY) * (cX - aX)) );
  int returnVal;
  if(eq > 0){
    returnVal = 1; //ASSERT: point is above line
  }
  else if(eq < 0){
    returnVal = -1;//ASSERT: point is below line.
  }
  else{
    returnVal = 0; //ASSERT:point must be on the line.
  }

  return returnVal;
  
}




//PRE:all three LinkedLists are of type Point and satisfy the CI, leftMost and rightMost are points representing the leftmost and rightmost points in a linkedList. below and above must be passed by reference because they are being changed.
//POST:puts all points that are below the line created be leftmost and rightmost into one linkedlist and all others above into another linkedlist.
void allocateAboveOrBelow(const LinkedList <Point> & original, LinkedList <Point> & below, LinkedList <Point> & above, Point leftMost, Point rightMost){
  int numPoints = original.getNumNodes();
  for(int index = 0; index < numPoints; index++){
    Point pPoint = original.getNth(index);
    if((pPoint != leftMost)  && (pPoint != rightMost)){
      int loc = aboveOrBelow(leftMost,rightMost,pPoint);
      if(loc == 1){//ASSERT:pPoint is not rightMost or leftMost.
	above.addToBack(pPoint);//ASSERT:aboveOrBelow returned 1 so it must be above or on the line so add it to above linkedlist.
      }
      else if(loc == -1){//ASSERT:aboveOrBelow returned -1 so it must be below the line so add it to below linkedlist
	below.addToBack(pPoint);
      }
      else{//Point must be on line so add to both lists, this way if it is only point it will get added to convex hull, else it wont matter and will get taken out in recursion, so really just adding to the list in the case that it is the only point going to the next recursion where it will be base case..
	above.addToBack(pPoint);
	below.addToBack(pPoint);
      }
    }
  }
}




//PRE:LinkedList satisfies the CI and has 3 or more nodes in it and is of type point
//POST:prints out the convex hull of a group of points.
LinkedList <Point> quickHull(const LinkedList <Point> & pLinkedList){
  bool above = true;
  bool below = false;
  LinkedList <Point> aboveList;
  LinkedList <Point> belowList;
  LinkedList <Point> convexHull;
  LinkedList <Point> aboveConvexHull;
  LinkedList <Point> belowConvexHull;
  if(pLinkedList.getNumNodes() == THREE){//if there are only three points in the linkedlist they must be the convex hull so just output them. 
    convexHull = pLinkedList;//ASSERT:there are only three points in the linkedList
  }
  else{
    Point rightMost = returnRightMost(pLinkedList);//getRightMost point in list
    Point leftMost = returnLeftMost(pLinkedList);//getLeftMost point in list
    //cerr<<"rightmost "<<rightMost<<" leftmost " <<leftMost<<endl;
    allocateAboveOrBelow(pLinkedList,belowList,aboveList,leftMost,rightMost);//put points above line created by leftmost and rightmost in abovelist and below line in belowlist
    aboveConvexHull = quickHullAbove(aboveList,leftMost,rightMost);//use quickHullHelper for everything above or on the line.
    belowConvexHull = quickHullBelow(belowList,leftMost,rightMost);//use quickHullHelper for everything below the line.
    belowConvexHull.addToFront(leftMost);//start counterclockwise with leftmost point at front of belowConvexHull list
    aboveConvexHull.addToFront(rightMost);//also start aboveConvexHull list with rightmost
    convexHull = belowConvexHull + aboveConvexHull;//then add below first and right second.
    
  }
  return convexHull;
}


//PRE:pLinkedList is a linkedList of points that satisfies the CI, leftmost is the leftmost point in the linked list, rightMost is the rightmost, the bool location is to explain what the function is looking for, points above or below the line created by leftmost and rightmost, false is below, true is above.
//POST:recursively calls this function to get the convex hull of points under or above leftmost and rightmost.
LinkedList <Point> quickHullAbove(const LinkedList<Point> & pLinkedList,Point leftMost,Point rightMost){
  LinkedList <Point> belowList;
  LinkedList <Point> aboveListRight;
  LinkedList <Point> aboveListLeft;
  LinkedList <Point> hullAbove;
  Point highestPoint;
  int numNodes = pLinkedList.getNumNodes();
  if(numNodes == 0){
  }//do nothing
  else if(numNodes == 1){//these two are base cases
    hullAbove = pLinkedList;
  }//this must be a convex hull point
  else{
    highestPoint = returnHighest(pLinkedList);//must be a convex hull point
    
    allocateAboveOrBelow(pLinkedList,belowList,aboveListRight,highestPoint,rightMost);//put points above line created by leftmost and rightmost in abovelist and below line in belowlist
    allocateAboveOrBelow(pLinkedList,belowList,aboveListLeft,leftMost,highestPoint);//put points above line created by leftmost and rightmost in abovelist and below line in belowlist
    //ASSERT:now belowlist1 and belowlist2 hold the points below the line made by rightmost and lowestpoint and leftmost and lowestpoint
    LinkedList <Point> hullAboveRight = quickHullAbove(aboveListRight,highestPoint,rightMost);
    LinkedList <Point> hullAboveLeft = quickHullAbove(aboveListLeft,leftMost,highestPoint);
    hullAboveRight.addToBack(highestPoint);//must be a piece of convex hull so add to end of hullaboveright.
    hullAbove = hullAboveRight + hullAboveLeft;//start with right side of hull to do counterclockwise
    
  }
  return hullAbove;
}



//PRE:pLinkedList is a linkedList of points that satisfies the CI, leftmost is the leftmost point in the linked list, rightMost is the rightmost point in the linkedlist.
//POST:recursively calls this function to get the convex hull of points below the line created by leftMost and rightMost point.
LinkedList <Point> quickHullBelow(const LinkedList<Point> & pLinkedList,Point leftMost,Point rightMost){
  LinkedList <Point> aboveList;
  LinkedList <Point> belowListRight;
  LinkedList <Point> belowListLeft;
  LinkedList <Point> hullBelow;
  Point lowestPoint;
  int numNodes = pLinkedList.getNumNodes();
  if(numNodes == 0){//first base case
  }//do nothing
  else if(numNodes == 1){//second base case
    hullBelow = pLinkedList;//this must be a convex hull point
  }
  else{
    lowestPoint = returnLowest(pLinkedList);//must be a convex hull point
    
    allocateAboveOrBelow(pLinkedList,belowListRight,aboveList,lowestPoint,rightMost);//put points above line created by leftmost and rightmost in abovelist and below line in belowlist
    allocateAboveOrBelow(pLinkedList,belowListLeft,aboveList,leftMost,lowestPoint);//put points above line created by leftmost and rightmost in abovelist and below line in belowlist
    //ASSERT:now belowlist1 and belowlist2 hold the points below the line made by rightmost and lowestpoint and leftmost and lowestpoint
    LinkedList <Point> hullBelowRight = quickHullBelow(belowListRight,lowestPoint,rightMost);
    LinkedList <Point> hullBelowLeft = quickHullBelow(belowListLeft,leftMost,lowestPoint);
    hullBelowLeft.addToBack(lowestPoint);//needs to be added after end of left side of hull to make it counterclockwise
    hullBelow = hullBelowLeft + hullBelowRight; //start from leftside so its counterclockwise
  }
  return hullBelow;
}
